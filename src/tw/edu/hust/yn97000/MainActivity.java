package tw.edu.hust.yn97000;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.view.Menu;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        HttpPost request = new HttpPost("http://maps.googleapis.com/maps/api/geocode/json?address=taichung&sensor=true");
        
        try{
        	HttpResponse response = new DefaultHttpClient().execute(request);
        	
        	if( response.getStatusLine().getStatusCode() == 200){
        		String result = new String(EntityUtils.toString(response.getEntity()).getBytes("UTF8"));
        		
        		Toast.makeText(MainActivity.this, result, Toast.LENGTH_LONG).show();
        	}
        } catch (Exception e) {
        	e.printStackTrace();
		}
    }


	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}
